﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (RectTransform), typeof(Image))]
public class Item : MonoBehaviour {

    /// <summary>
    /// Item index, public to look at inspector
    /// </summary>
    public int index;
    /// <summary>
    /// Progress of item's fade
    /// </summary>
    public float progress;

    /// <summary>
    /// Rect transform of the item
    /// </summary>
    private RectTransform rectTransform;
    /// <summary>
    /// Image of the item
    /// </summary>
    private Image image;

    /// <summary>
    /// Position in center
    /// </summary>
    private float startPosition;
    /// <summary>
    /// Position on edge
    /// </summary>
    private float endPosition;

    /// <summary>
    /// Scale in center
    /// </summary>
    private Vector2 startScale;
    /// <summary>
    /// Scale on edge
    /// </summary>
    private Vector2 endScale;

    /// <summary>
    /// Alpha in center
    /// </summary>
    private float startAlpha;
    /// <summary>
    /// Alpha on edge
    /// </summary>
    private float endAlpha;

    /// <summary>
    /// Set item index
    /// </summary>
    /// <param name="index"></param>
    public void Setindex(int index)
    {
        this.index = index;
    }

    /// <summary>
    /// Get item index
    /// </summary>
    /// <returns></returns>
    public int GetIndex()
    {
        return index;
    }

    /// <summary>
    /// Set progress
    /// </summary>
    /// <param name="progress">0 is currently visible item</param>
    public void SetProgress(float progress)
    {
        this.progress = progress;
        transform.localPosition = new Vector2(transform.localPosition.x, Mathf.Lerp(startPosition, endPosition, Mathf.Abs(this.progress)));
        transform.localScale = Vector2.Lerp(startScale, endScale, Mathf.Abs(this.progress));
        image.color = new Color(image.color.r, image.color.g, image.color.b, Mathf.Lerp(startAlpha, endAlpha, Mathf.Abs(this.progress)));
    }

    /// <summary>
    /// Get progress
    /// </summary>
    /// <returns></returns>
    public float GetProgress()
    {
        return progress;
    }

    /// <summary>
    /// Set position
    /// </summary>
    /// <param name="position"></param>
    public void SetPosition(Vector2 position)
    {
        rectTransform.localPosition = position;
    }

    /// <summary>
    /// Get position
    /// </summary>
    /// <returns></returns>
    public Vector2 GetPosition()
    {
        return rectTransform.localPosition;
    }

    /// <summary>
    /// Hide element
    /// </summary>
    public void Hide()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Show element
    /// </summary>
    public void Show()
    {
        gameObject.SetActive(true);
    }

	// Use this for initialization
	void Awake () {
        startPosition = 1f;
        endPosition = 0.75f;
        startScale = new Vector2(1.2f, 1.2f);
        endScale = new Vector2(0.9f, 0.9f);
        startAlpha = 1f;
        endAlpha = 0.4f;
        rectTransform = GetComponent<RectTransform>();
        image = GetComponent<Image>();
    }
}
