﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainController : MonoBehaviour {

    /// <summary>
    /// Container for our Scroll Component
    /// </summary>
    GameObject scrollContainer;
    /// <summary>
    /// Title to show which element we choose
    /// </summary>
    Text title;
    /// <summary>
    /// ScrollComponent itself
    /// </summary>
    ScrollComponent scroller;

	// Use this for initialization
	void Start () {
        title = GameObject.Find("Title").GetComponent<Text>();
        scrollContainer = GameObject.Find("Scroll");
        scroller = scrollContainer.AddComponent<ScrollComponent>().Setup(32, ItemSelected);
    }

    void ItemSelected(int index)
    {
        title.text = "Element " + index;
    }
}
