﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScrollComponent : MonoBehaviour {

    /// <summary>
    /// How much items do we have?
    /// </summary>
    public int ItemsCount;

    /// <summary>
    /// ScrollView
    /// </summary>
    private ScrollView scrollView;
    /// <summary>
    /// ScrollView items
    /// </summary>
    private List<Item> scrollViewItems;

    /// <summary>
    /// Item Selected event delegate
    /// </summary>
    /// <param name="index"></param>
    public delegate void ItemSelected(int index);

    /// <summary>
    /// Setup ScrollView component
    /// </summary>
    /// <param name="itemsCount">how much items do we want to create</param>
    /// <returns></returns>
    public ScrollComponent Setup(int itemsCount, ItemSelected clickListener)
    {

        ItemsCount = itemsCount;

        if (ItemsCount <= 0)
            throw new UnityException("ScrollComponent must have more than 0 items");
            

        ScrollView scrollView = (Instantiate(Resources.Load("prefabs/ScrollView"), transform) as GameObject).GetComponent<ScrollView>();
        List<Item> scrollViewItems = new List<Item>(ItemsCount);
        for (int i = 0; i < ItemsCount; i++)
        {
            scrollViewItems.Add((Instantiate(Resources.Load("prefabs/ScrollViewItem"), scrollView.ScrollRoot.transform) as GameObject).GetComponent<Item>());
        }

        scrollView.Setup(scrollViewItems, clickListener);

        return this;
    }

}
