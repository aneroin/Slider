﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScrollView : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

    /// <summary>
    /// Scroll root object, to move content
    /// </summary>
    public GameObject ScrollRoot;
    /// <summary>
    /// List of all items, contained in this view
    /// </summary>
    public List<Item> items;
    /// <summary>
    /// Speed of the snapping effect
    /// </summary>
    public float speed;

    /// <summary>
    /// Current snap coroutine, to intercept once needed
    /// </summary>
    private Coroutine runningCoroutine;
    /// <summary>
    /// Callback on item selection
    /// </summary>
    private ScrollComponent.ItemSelected onItemSelected;
    /// <summary>
    /// Current screen size
    /// </summary>
    private Vector2 ScreenSize;

    /// <summary>
    /// Position field
    /// </summary>
    private float _currentPosition;
    /// <summary>
    /// Position property. Normalized position is recalculated automatically.
    /// </summary>
    public float currentPosition
    {
        get
        {
            return _currentPosition;
        }

        set
        {
            _currentPosition = value;
            _currentPositionNormalized = NormalPosition(_currentPosition);
            ScrollRoot.transform.localPosition = new Vector3(currentPosition, 0f, 0f);
            for (int i = 0; i < items.Count; i++)
            {
                items[i].SetProgress(items[i].GetIndex() + _currentPositionNormalized);
            }
            HideExplicitItems(-(int)_currentPositionNormalized);
        }
    }

    /// <summary>
    /// Normalized position field
    /// </summary>
    private float _currentPositionNormalized;
    /// <summary>
    /// Normalized position property. Position is recalculated automatically.
    /// </summary>
    public float currentPositionNormalized
    {
        get
        {
            return _currentPositionNormalized;
        }

        set
        {
            _currentPositionNormalized = value;
            _currentPosition = AbsolutePosition(_currentPositionNormalized);
            ScrollRoot.transform.position = new Vector3(currentPosition, 0f, 0f);
            for (int i = 0; i < items.Count; i++)
            {
                items[i].SetProgress(items[i].GetIndex() + _currentPositionNormalized);
            }
            HideExplicitItems(-(int)_currentPositionNormalized);
        }
    }

    /// <summary>
    /// List of items
    /// </summary>
    List<Item> ScrollItems = new List<Item>();

    /// <summary>
    /// When drag occurs
    /// </summary>
    /// <param name="eventData"></param>
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (runningCoroutine!=null)
            StopCoroutine(runningCoroutine);
    }

    /// <summary>
    /// When drag proceeds
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrag(PointerEventData eventData)
    {
        currentPosition += eventData.delta.x;
    }

    /// <summary>
    /// When drag ends
    /// </summary>
    /// <param name="eventData"></param>
    public void OnEndDrag(PointerEventData eventData)
    {
        runningCoroutine = StartCoroutine(SnapToClosest());
    }

    /// <summary>
    /// Normalize
    /// </summary>
    /// <param name="AbsolutePosition"></param>
    /// <returns></returns>
    float NormalPosition(float AbsolutePosition)
    {
        return AbsolutePosition / (ScreenSize.x * 0.5f) ;
    }

    /// <summary>
    /// Reverse normalize
    /// </summary>
    /// <param name="NormalPosition"></param>
    /// <returns></returns>
    float AbsolutePosition(float NormalPosition)
    {
        return NormalPosition * (ScreenSize.x * 0.5f);
    }


    public void Awake()
    {
        ScrollRoot = transform.GetChild(0).gameObject;
    }

    /// <summary>
    /// Setup ScrollView
    /// </summary>
    /// <param name="items"></param>
    /// <param name="onItemSelected"></param>
    public void Setup(List<Item> items, ScrollComponent.ItemSelected onItemSelected)
    {
        ScreenSize = new Vector2(Camera.main.pixelWidth, Camera.main.pixelHeight);
        this.items = items;
        for (int i = 0; i < this.items.Count; i++)
        {
            this.items[i].Setindex(i);
            this.items[i].SetPosition(new Vector2(AbsolutePosition(i), 0f));
            this.items[i].SetProgress(1f);
        }
        this.items[0].SetProgress(0f);
        HideExplicitItems(0);
        this.onItemSelected += onItemSelected;

    }

    /// <summary>
    /// Snap closest item on drag ended
    /// </summary>
    /// <returns></returns>
    IEnumerator SnapToClosest()
    {
            float targetRoot;
            float targetRootNormalized;

            float deltaRoot = currentPositionNormalized - Mathf.Floor(currentPositionNormalized);

            if (Mathf.Approximately(deltaRoot, 0f)){
                targetRootNormalized = (int) currentPositionNormalized;
            } else if (deltaRoot <= 0.5f)
            {
                targetRootNormalized = Mathf.Floor(currentPositionNormalized);
            } else
            {
                targetRootNormalized = Mathf.Ceil(currentPositionNormalized);
            }


            if (targetRootNormalized > 0f)
                targetRootNormalized = 0f;
            else if (targetRootNormalized < -items.Count+1)
                targetRootNormalized = -items.Count + 1;

            targetRoot = AbsolutePosition(targetRootNormalized);

            float t = 0f;
            float diff = targetRoot - currentPosition;
            float origin = currentPosition;

        yield return null;

        while (!Mathf.Approximately(1f, t) && t <= 1f)
        {
            t += Time.deltaTime * speed;
            currentPosition = origin + diff * Mathf.Min(1f, t);
            yield return new WaitForEndOfFrame();
        }

        if (onItemSelected != null)
            onItemSelected(-(int)targetRootNormalized);

        yield return true;
    }


    /// <summary>
    /// Hide explicit items
    /// </summary>
    /// <param name="index"></param>
    public void HideExplicitItems(int index)
    {
        //Issue: Possible array out of bounds exception, targetting index higher than items list Count.
        //Solution: clamp index between 0 and items.Count - 1.
        index = Mathf.Clamp(index, 0, items.Count-1);

        int leftBound, rightBound;

        leftBound = index < 2 ? - 1 : index - 2; ;
        rightBound = index > items.Count - 2 ? -1 : index + 3; ;

        if (leftBound > 0)
            for (int i = 0; i < leftBound; i++)
            {
                items[i].Hide();
            }
        if (rightBound > 0)
            for (int i = rightBound; i < items.Count; i++)
            {
                items[i].Hide();
            }
        for (int i = (leftBound >= 0 ? leftBound : 0); i < (rightBound < items.Count ? rightBound : items.Count); i++)
        {
            items[i].Show();
        }
    }
}
